﻿layui.define(function (exports) {
    var setter = layui.setter
        ,admin = layui.admin;
    /**
    * 验证会话是否有效
    * */
    var CheckSession = function () {
        admin.req({
            type: 'post',
            url: '/Check/CheckSession',
            data: {
                ssoToken: layui.data(setter.tableName)["token"]
            },
            success: function (res) {
                if (res.code != 0) {
                    window.location.href = window.location.protocol + "//" + window.location.host + "/Login/Check?returnurl=" + window.location.href;
                }
            }
        });
    }

    var CheckLoginStatus = function () {
        //本地客户端没有token，跳转中间页获取token并check、登录
        if (layui.data(setter.tableName)["token"] == undefined || layui.data(setter.tableName)["token"] == "") {
            window.location.href = window.location.protocol + "//" + window.location.host + "/Login/Check?returnurl=" + window.location.href;
        } else {
            CheckSession();
        }
    }

    CheckLoginStatus();

    exports("checklogin", {});
});