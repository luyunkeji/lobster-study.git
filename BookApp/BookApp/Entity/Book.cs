﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace BookApp.Entity
{
    [Table("Books")]
    public class BookEntity
    {
        [Key]
        public int Id { get; set; }

        public string BookName { get; set; }
        public decimal BuyPrice { get; set; }
        public DateTime BuyDate { get; set; }
        public int Flag { get; set; }
    }
}
