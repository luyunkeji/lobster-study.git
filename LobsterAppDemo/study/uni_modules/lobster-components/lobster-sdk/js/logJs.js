export default {

	/**
	 * log对象
	 */
	logEntity: {
		id:0, //编号
		time:'',//时间
		msg:'',//内容
		reqdata:'',//请求参数
		respdata:'',//返回内容
		type:0, //0普通日志 1:http请求日志  2错误日志
 	},
	//log列表
	logs: [],

	/**
	 * 设置对象返回值
	 * @param {Object} logId
	 * @param {Object} repData
	 */
	setrespdata: function(logId, repData) {
		let index = this.logs.findIndex(p => p.id == logId);
		this.logs[index].respdata = repData;
	},
	clearLog:function(){
		this.logs=[];
	},
}
