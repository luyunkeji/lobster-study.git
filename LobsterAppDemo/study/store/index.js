import Vue from 'vue'
import Vuex from 'vuex'

import group from '@/uni_modules/lobster-components/modules/group'
import user from '@/uni_modules/lobster-components/modules/user'
import login from '@/uni_modules/lobster-components/modules/login.js'
import im from '@/uni_modules/lobster-components/lobster-sdk/js/im.js'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		login, 
		group,
		user, 
		im
	}
})
