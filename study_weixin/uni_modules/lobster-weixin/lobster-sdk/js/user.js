import http from './http.js'
import config from './config.js'
import store from '@/store'
export default {
	
	/**
	 * @description  获取用户信息
	 * @param {Object} userId
	 */
	getuserinfo(userId){
		return http.GET(config.apiGateway +'/sso/v1/MPUser/GetMPUser', {
			userId:userId
		});
	},
	/**
	 * @description 更新用户头像
	 * **/
	updateUserImgUrl(userimgurl) {
		console.log(store.state.login.userId);
		return http.POST(config.apiGateway + '/self-patient/v1/User/UpdateUserImgUrl', {
			workId: config.workId,
			mpUserId: store.state.login.userId, 
			userimgurl: userimgurl
		});
	},
	//更改昵称
	editNickName(kname) {
		return http.GET(config.apiGateway +'/sso/v1/MPUser/EditNickName', {
			mpUserId: store.state.login.userId,
			nickName: kname,
		})
	},
	/**
	 * @description 更新用户名称
	 * **/
	updateUserName(username) {
		return http.GET(config.apiGateway +'/self-patient/v1/User/UpdateUserName', {
			workId: config.workId,
			mpUserId: store.state.login.userId, 
			userName: username
		});
	},
	/**
	 * @description 更新用户性别
	 * **/
	updateUserSex(userSex) {
		return http.GET(config.apiGateway +'/self-patient/v1/User/UpdateUserSex', {
			workId: config.workId,
			mpUserId: store.state.login.userId, 
			userSex: userSex
		});
	},
	/**
	 * @description 更新用户生日
	 * **/
	updateUserBirthday(birthday) {
		return http.GET(config.apiGateway +'/self-patient/v1/User/UpdateUserBirthday', {
			workId: config.workId,
			mpUserId: store.state.login.userId, 
			userbirthday: birthday
		});
	},
}
