export default {
	formatNumber(n) {
		n = n.toString()
		return n[1] ? n : '0' + n
	},
	//得到时间格式2018-10-02
	formatDate(date) {
		// uni.showToast({
		// 	title:"日期",
		// 	duration:1000,
		// })
		// uni.showToast({
		// 	title:"日期1"+JSON.stringify(date),
		// 	duration:2000,
		// })
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		// uni.showToast({
		// 	duration:3000,
		// 	title:"日期2"+year+""+month+""+day
		// })
		//var result=[year, month, day].map(this.formatNumber).join('-');
		// uni.showToast({
		// 	title:result
		// })
		return [year, month, day].map(this.formatNumber).join('-');
	},
	
	getDates(days, todate) {
		var dateArry = [];
		for (var i = 0; i < days; i++) {
			var dateObj = this.dateLater(todate, i);
			dateArry.push(dateObj)
		}
		return dateArry;
	},
	
	dateLater(dates, later) {
		let dateObj = {};
		let show_day = new Array('周日', '周一', '周二', '周三', '周四', '周五', '周六');
		let date = new Date(dates);
		date.setDate(date.getDate() + later);
		let day = date.getDay();
		let yearDate = date.getFullYear();
		let month = ((date.getMonth() + 1) < 10 ? ("0" + (date.getMonth() + 1)) : date.getMonth() + 1);
		let dayFormate = (date.getDate() < 10 ? ("0" + date.getDate()) : date.getDate());
		dateObj.time = yearDate + '-' + month + '-' + dayFormate;
		dateObj.timemonth = month + '-' + dayFormate;
		dateObj.timeday = dayFormate;
		dateObj.week = show_day[day];
		return dateObj;
	},
	
	formatDateMonth(month) {
		let dateObj = '';
		dateObj = month.split('-')[1] + '月' + month.split('-')[2] + '日';
		return dateObj
	},
	navigateBack(delta = 1) {
		uni.navigateBack({
			delta: delta
		})
	},
	switchTab(url, success) {
		uni.switchTab({
			url: url,
			success: function() {
				success && success();
			}
		})
	},
	redirectTo(url, success, fail) {
		uni.redirectTo({
			url: url,
			success: function() {
				if (success) {
					success();
				}
			},
			fail: function() {
				if (fail) {
					fail();
				}
			}
		})
	},
	navigateTo(url, sucess, fail) {
		uni.navigateTo({
			url: url,
			success: function() {
				if (sucess) {
					sucess();
				}
			},
			fail: function(res) {
				fail&&fail();
				console.log(res);
			}
		})
	},
	reLaunch(url) {
		uni.reLaunch({
			url: url
		})
	},
	showToast(title, duration = 1500, mask = true) {
		uni.showToast({
			title: title,
			duration: duration,
			icon: 'none',
			mask: mask
		})
	},
	showModal(title, content, confirm, cancel, showCancel = true) {
		uni.showModal({
			title: title,
			content: content,
			showCancel: showCancel,
			success: (res) => {
				if (res.confirm) {
					confirm && confirm();
				} else if (res.cancel) {
					cancel && cancel();
				}
			}
		})
	},
	showLoading(title, mask = true) {
		uni.showLoading({
			title: title,
			mask: mask
		})
	},
	navigateBack(detal = 1, success, fail) {
		var currentPages = getCurrentPages();
		if (currentPages.length > detal) {
			uni.navigateBack({
				delta: detal
			})
			if (success)
				success();
		} else {
			if (fail)
				fail();
		}
	},
	randomString(range){
	    var str = "",
	        arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
	 
	    for(var i=0; i<range; i++){
	        var pos = Math.round(Math.random() * (arr.length-1));
	        str += arr[pos];
	    }
	    return str;
	},
	//时长转换器
	formateSeconds(duration) {
		if(!duration) return '';
		let secondTime = parseInt(duration) //将传入的秒的值转化为Number
		let min = 0 // 初始化分
		let h = 0 // 初始化小时
		let result = ''
		if (secondTime >= 60) { //如果秒数大于60，将秒数转换成整数
			min = parseInt(secondTime / 60) //获取分钟，除以60取整数，得到整数分钟
			secondTime = parseInt(secondTime % 60) //获取秒数，秒数取佘，得到整数秒数
			if (min >= 60) { //如果分钟大于60，将分钟转换成小时
				h = parseInt(min / 60) //获取小时，获取分钟除以60，得到整数小时
				min = parseInt(min % 60) //获取小时后取佘的分，获取分钟除以60取佘的分
			}
		}
		//result =`${h.toString().padStart(2,'0')}:${min.toString().padStart(2,'0')}:${secondTime.toString().padStart(2,'0')}`
		if (h > 0) {
			result = result + `${h.toString()}时`;
		}
		if (min > 0) {
			result = result + `${min.toString()}分`;
		}
		if (secondTime > 0) {
			result = result + `${secondTime.toString()}秒`;
		}
		return result;
	},
	
	/**
	 * @description  获取当前时间  时分秒
	 */
	getCurrentTime() {
		var date =new Date(); 
		let yy = date.getFullYear();
		let mm = date.getMonth() + 1;
		let dd = date.getDate();
		let hh =date.getHours();
		let mf = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
		let ss = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
		return yy + '/' + mm + '/' + dd + ' ' + hh + ':' + mf + ':' + ss;
	},
}
