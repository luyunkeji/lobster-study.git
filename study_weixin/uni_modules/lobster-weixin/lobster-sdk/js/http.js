export default {
	RequestData(url, method, data, content_type) {
		// if (url.indexOf("http") != 0) {
		// 	url = config.apiGateway + url;
		// }
		var ssotoken = uni.getStorageSync('ssoToken');
		var header = {
			'content-type': content_type
		};
		if (ssotoken) {
			header = {
				'content-type': content_type,
				'Authorization': 'Bearer ' + ssotoken
			}
		}
		return new Promise((resolve, reject) => {
			uni.request({
				url: url,
				data: data,
				header: header,
				method: method,
				success: (res) => {
					if (res.data.code == 0) {
						resolve(res.data);
					} else {
						reject(res.data);
					}
				},
				fail: (res) => {
					reject({
						code: 500,
						msg: res.errMsg
					})
				}
			})
		})
	},
	GET(url, data, content_type = 'application/json') {

		return this.RequestData(url, 'GET', data, content_type);
	},
	POST(url, data, content_type = 'application/json') {
		return this.RequestData(url, 'POST', data, content_type)
	}
}
