const loginModules={
	state: {
		hasLogin: uni.getStorageSync('hasLogin') || false,
		userId: uni.getStorageSync('userId') || '',
		ssoToken:uni.getStorageSync('ssoToken')||'',
		openId:uni.getStorageSync('openId')||'',
	},
	mutations: {
		login(state, data) {
			state.hasLogin = true;
			state.userId = data.userId;
			state.ssoToken=data.ssoToken;
			state.openId=data.openId;
			uni.setStorageSync('hasLogin', true);
			uni.setStorageSync('userId', data.userId);
			uni.setStorageSync('ssoToken', data.ssoToken);
			uni.setStorageSync('openId', data.openId);
		},
		logout(state) {
			state.hasLogin = false;
			state.userId = '';
			state.ssoToken='';
			state.openId='';
			uni.setStorageSync('hasLogin', false);
			uni.removeStorageSync('userId');
			uni.removeStorageSync('ssoToken');
			uni.removeStorageSync('openId');
		}
	}
}
export default loginModules