import Vue from 'vue'
import Vuex from 'vuex'

import login from '@/uni_modules/lobster-weixin/modules/login.js'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		login
	}
})
